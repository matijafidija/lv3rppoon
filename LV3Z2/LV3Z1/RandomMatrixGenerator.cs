﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3Z1
{
    public class RandomMatrixGenerator
    {
        private static RandomMatrixGenerator instance;
        private Random generator;
        private RandomMatrixGenerator()
        {
            this.generator = new Random();
        }
        public static RandomMatrixGenerator GetInstance()
        {
            if (instance == null)
            {
                instance = new RandomMatrixGenerator();
            }
            return instance;
        }
        public double[,] Dimensions(int rows, int colums)
        {
            double[,] matrix = new double[rows, colums];
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < colums; j++)
                {
                    matrix[i, j] = generator.NextDouble();
                }
            }
            return matrix;
        }
    }
}
