﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3Z1
{
    class Program
    {
        static void Main(string[] args)
        {
            RandomMatrixGenerator Matrix = RandomMatrixGenerator.GetInstance();
            double[,] matrix = Matrix.Dimensions(20, 20);
            for (int i = 0; i < 20; i++)
            {
                for (int j = 0; j < 20; j++)
                {
                    Console.Write(matrix[i, j] + " ");
                }
                Console.WriteLine();
            }
        }
    }
}
