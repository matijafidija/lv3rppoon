﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3Z4
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleNotification notification = new ConsoleNotification("Micho", "Labos", "Provjera", DateTime.Now, Category.INFO, ConsoleColor.Cyan);
            NotificationManager manager = new NotificationManager();
            manager.Display(notification);
        }
    }
}
